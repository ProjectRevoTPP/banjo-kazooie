#include <ultra64.h>
#include "functions.h"
#include "variables.h"

extern u8 D_803A5D00[2][0x1ecc0];

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C160.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C180.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C1A0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C204.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C27C.s")

void func_8030C2D4(Gfx **gdl, Mtx **mptr, Vtx **vptr){
    func_80254348();
    func_80253640(gdl, D_803A5D00[func_8024BDA0()]);
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C33C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C704.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_851D0/func_8030C710.s")
